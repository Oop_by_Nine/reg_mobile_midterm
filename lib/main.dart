import 'dart:async';

import 'package:flutter/material.dart';
import 'package:nine/screen/MainScreen.dart';

StreamController<int> streamController = StreamController<int>();

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          textTheme: const TextTheme(
            bodyText2: TextStyle(color: Colors.white),
          ),
        ),
        home: MainScreen(streamController.stream));
  }
}
