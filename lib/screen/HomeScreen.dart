import 'package:flutter/material.dart';
import 'package:nine/main.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF717D8C),
        body: Column(
          children: [
            Container(
              decoration: const BoxDecoration(color: Color(0xFF2F343A)),
              child: Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: Image.asset(
                      'assets/images/buu.png',
                      height: 52,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Container(
                      width: double.infinity,
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: const [
                        CircleAvatar(
                          radius: 20,
                          backgroundImage: NetworkImage(
                            'https://cdn.discordapp.com/attachments/948627865329610752/1070271558288949309/photo-1507525428034-b723cf961d3e-1024x681.jpg',
                          ),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text("Akapon"),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 30),
              child: Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        print("About BUU");
                      },
                      borderRadius: BorderRadius.circular(15),
                      splashColor: Colors.white.withOpacity(0.1),
                      highlightColor: Colors.white.withOpacity(0.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/about.png',
                            width: 36,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Text("About BUU")
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        print("BUU News");
                      },
                      borderRadius: BorderRadius.circular(15),
                      splashColor: Colors.white.withOpacity(0.1),
                      highlightColor: Colors.white.withOpacity(0.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/news.png',
                            width: 36,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Text("BUU News")
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        print("Event");
                      },
                      borderRadius: BorderRadius.circular(15),
                      splashColor: Colors.white.withOpacity(0.1),
                      highlightColor: Colors.white.withOpacity(0.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/event.png',
                            width: 36,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Text("Event")
                        ],
                      ),
                    ),
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        streamController.add(2);
                      },
                      borderRadius: BorderRadius.circular(15),
                      splashColor: Colors.white.withOpacity(0.1),
                      highlightColor: Colors.white.withOpacity(0.0),
                      child: Column(
                        children: [
                          Image.asset(
                            'assets/images/student.png',
                            width: 26,
                          ),
                          const SizedBox(
                            height: 8,
                          ),
                          const Text("Student")
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 30,
              ),
              child: Container(
                constraints: const BoxConstraints(
                  minWidth: 395,
                ),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Color(0xFF2F343A),
                ),
                child: const Padding(
                  padding: EdgeInsets.only(top: 10, left: 13, bottom: 10),
                  child: Text("Schedule (Tuesday)"),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                  color: Color(0xFF2F343A),
                ),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    children: [
                      ScheduleCard(
                        subject: 'Miltimedia Programming for Multiplatforms',
                        time: 'IF-4C02(10.00-12.00)',
                      ),
                      const SizedBox(
                        height: 9,
                      ),
                      ScheduleCard(
                        subject: 'Algorithm Design and Application',
                        time: 'IF-6T04(15.00-17.00) IF-4C02(17.00-18.00)',
                      )
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 25,
              ),
              child: Container(
                constraints: const BoxConstraints(
                  minWidth: 395,
                ),
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  color: Color(0xFF2F343A),
                ),
                child: Padding(
                  padding: EdgeInsets.only(top: 6, left: 13, bottom: 6),
                  child: Text(
                    "News/Activities\nมกราคม 2566",
                  ),
                ),
              ),
            ),
            NewsCard(),
          ],
        ),
      ),
    );
  }

  Padding NewsCard() {
    return Padding(
      padding: const EdgeInsets.all(10),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.network(
            'https://media.discordapp.net/attachments/911744953036709940/1070219613641654302/image.png',
            width: 167,
          ),
          SizedBox(
            width: 8,
          ),
          Flexible(
            child: Container(
              decoration: const BoxDecoration(
                color: Color(0xFF2F343A),
                borderRadius: BorderRadius.all(Radius.circular(10)),
              ),
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(6),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: const [
                      Text(
                        "การแข่งขันกีฬาบูรพาเกมส์ 2022",
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Text(
                        "เตรียมตัวพบกับการแข่งขันกีฬาเชื่อมความสัมพันธ์ระหว่าง 17 คณะ 1 วิทยาลัย",
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Container ScheduleCard({required String subject, required String time}) {
    return Container(
      constraints: const BoxConstraints(
        minWidth: 375,
        maxWidth: 375,
      ),
      decoration: const BoxDecoration(
        color: Color(0xFF80CEB9),
        borderRadius: BorderRadius.all(Radius.circular(15)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(7),
        child: Row(
          children: [
            Flexible(
              flex: 2,
              child: Text(
                subject,
              ),
            ),
            Expanded(
              child: Container(
                width: double.infinity,
              ),
            ),
            Flexible(
              flex: 2,
              child: Text(
                time,
              ),
            )
          ],
        ),
      ),
    );
  }
}
