import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/material.dart';

class StudentScreen extends StatelessWidget {
  const StudentScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Color(0xFF717D8C),
        body: Stack(
          children: [
            Column(
              children: [
                Container(
                  decoration: const BoxDecoration(color: Color(0xFF2F343A)),
                  child: Row(
                    children: [
                      Expanded(
                        flex: 1,
                        child: Image.network(
                          'https://media.discordapp.net/attachments/911744953036709940/1070236070135996427/Group_1.png',
                          height: 52,
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Container(
                          width: double.infinity,
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Row(
                          children: const [
                            CircleAvatar(
                              radius: 20,
                              backgroundImage: NetworkImage(
                                'https://cdn.discordapp.com/attachments/948627865329610752/1070271558288949309/photo-1507525428034-b723cf961d3e-1024x681.jpg',
                              ),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text("Akapon"),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                ProfileCard(
                  imageSource:
                      'https://cdn.discordapp.com/attachments/948627865329610752/1070271558288949309/photo-1507525428034-b723cf961d3e-1024x681.jpg',
                  fullname: 'Akapon Chayangkanont',
                  email: '62160317@go.buu.ac.th',
                  faculty: 'Informatics | Computer Science',
                ),
                SizedBox(
                  height: 20,
                ),
                Column(
                  children: [
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Color(0xFF2F343A),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 20),
                              child: Text('ลงทะเบียน'),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Color(0xFF2F343A),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 20),
                              child: Text('ผลการลงทะเบียน'),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Color(0xFF2F343A),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 20),
                              child: Text('ประวัติ'),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 25,
                    ),
                    Row(
                      children: [
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Color(0xFF2F343A),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 20),
                              child: Column(
                                children: [
                                  Text(
                                    'C.Reg',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text('115')
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Color(0xFF2F343A),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 20),
                              child: Column(
                                children: [
                                  Text(
                                    'C.Earn',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text('82')
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Color(0xFF2F343A),
                            ),
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(top: 20, bottom: 20),
                              child: Column(
                                children: [
                                  Text(
                                    'GPA',
                                    style: TextStyle(fontSize: 16),
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text('2.56')
                                ],
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                      ],
                    ),
                  ],
                )
              ],
            ),
            Positioned(
              bottom: 40,
              right: 10,
              child: Container(
                decoration: BoxDecoration(
                    color: Color(0xFF2F343A),
                    borderRadius: BorderRadius.all(Radius.circular(10))),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: InkWell(
                    onTap: () {
                      print('Schedule');
                    },
                    child: Row(
                      children: [
                        Image.asset(
                          'assets/images/schedule.png',
                          width: 36,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text('ตารางเรียน/ตารางสอบ')
                      ],
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Padding ProfileCard(
      {required String imageSource,
      required String fullname,
      required String email,
      required String faculty}) {
    return Padding(
      padding: const EdgeInsets.only(top: 40, left: 10, right: 10),
      child: Container(
        decoration: const BoxDecoration(
          color: Color(0xFF2F343A),
          borderRadius: BorderRadius.all(Radius.circular(10)),
        ),
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Stack(
            children: [
              Positioned(
                right: 8,
                top: -3,
                child: Image.asset(
                  'assets/images/grade.png',
                  width: 45,
                ),
              ),
              Row(
                children: [
                  CircleAvatar(
                    radius: 70,
                    backgroundImage: NetworkImage(
                      imageSource,
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        fullname,
                        style: TextStyle(fontSize: 18),
                      ),
                      Text(email),
                      Text(faculty),
                    ],
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
