import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:nine/screen/HomeScreen.dart';
import 'package:google_nav_bar/google_nav_bar.dart';
import 'package:nine/screen/StudentScreen.dart';
import 'dart:async';

class MainScreen extends StatefulWidget {
  const MainScreen(this.stream);
  final Stream<int> stream;

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int selectedScreen = 0;
  final List<Widget> _screen = [HomeScreen(), HomeScreen(), StudentScreen()];

  @override
  void initState() {
    super.initState();
    widget.stream.listen((index) {
      setScreen(index);
    });
  }

  void setScreen(index) {
    setState(() {
      selectedScreen = index;
      print(selectedScreen);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF2F343A),
      appBar: null,
      body: _screen[selectedScreen],
      bottomNavigationBar: Container(
        color: const Color(0xFF0D1117),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 10),
          child: GNav(
            gap: 8,
            tabBorderRadius: 15,
            backgroundColor: const Color(0xFF0D1117),
            activeColor: const Color(0xFFC9D1D9),
            tabBackgroundColor: const Color(0xFF0D1117),
            color: const Color(0xFFC9D1D9),
            padding: const EdgeInsets.all(0),
            onTabChange: (index) {
              setState(() {
                selectedScreen = index;
              });
            },
            iconSize: 50,
            tabs: const [
              GButton(
                active: true,
                icon: Icons.home,
              ),
              GButton(
                icon: Icons.login_rounded,
              )
            ],
          ),
        ),
      ),
    );
  }
}
